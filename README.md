# Nos cuidamos online

Web [nos cuidamos online](noscuidamos.online) de Tedic.

## Correr el servidor de desarrollo

```
docker compose up -d
```

Esto hará build y luego serve

Se accede en: http://localhost:8080/es/

## Hacer rebuild

Una vez corriendo puede ser necesario hacer build otra vez:

```
docker compose exec app npm run build
```
