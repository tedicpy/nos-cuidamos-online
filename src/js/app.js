const toggleMenu = document.querySelector(".navegacion-toggle");
const header = document.querySelector(".header");
const navegacion = document.querySelector(".navegacion");
const menuItems = document.querySelectorAll(".item-con-submenu");
const abrirMenu = document.querySelector("img.open");
const cerrarMenu = document.querySelector("img.close");
const submenu = document.querySelectorAll(".sub-menu");
// const containerMenu = document.querySelector(".nav__list");

toggleMenu.addEventListener("click", () => {
  header.classList.toggle("open");
  navegacion.classList.toggle("open");
  abrirMenu.classList.toggle("hide");
  cerrarMenu.classList.toggle("hide");
});

menuItems.forEach((item) => {
  item.addEventListener("click", () => {
    const submenu = item.querySelector(".sub-menu");
    const isActive = submenu.classList.contains("open");
    removeActiveClasses();

    if (!isActive) {
      submenu.classList.add("open");
    }
  });
});

function removeActiveClasses() {
  submenu.forEach((menuItem) => {
    menuItem.classList.remove("open");
  });
}

document.addEventListener("click", (e) => {
  if (!header.contains(e.target)) {
    submenu.forEach((menuItem) => {
      menuItem.classList.remove("open");
      navegacion.classList.remove("open");
      header.classList.remove("open");
      abrirMenu.classList.remove("hide");
      cerrarMenu.classList.add("hide");
    });
  }
});
