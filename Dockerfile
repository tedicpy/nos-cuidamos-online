FROM node:19

ENV DEBIAN_FRONTEND noninteractive
ENV CODE /usr/src/app

WORKDIR $CODE

# Copy code to conatiner volume
ADD . .

RUN export PATH="$PATH:$HOME/node_modules/.bin"

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/usr/local/bin/npx", "./node_modules/@11ty/eleventy", "--serve"]
