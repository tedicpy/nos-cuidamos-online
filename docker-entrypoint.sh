#!/bin/bash -e
app=/usr/src/app

if [ ! -d "$app/node_modules/@11ty/eleventy" ]; then
  echo -e "\nNo instalado\n"
  /usr/local/bin/yarn install
fi

npm run build

exec "$@"
